\begin{tikzpicture}
	\pgfmathsetmacro{\ex}{0}
	\pgfmathsetmacro{\ey}{5.5}

	\filldraw[color=gray!40, draw=black, rounded corners=3pt ] (0,0) rectangle (5,4);
	\node[text width=3cm] at (1.8,3.7) {Gateway};
	
	\draw[line width=0.6pt,black](0,3)--(0,5.5) node[anchor=north east]{};
	\draw (\ex,\ey) ++(45:.8) arc (45:-45:.8);
	\draw (\ex,\ey) ++(45:.6) arc (45:-45:.6);
	\draw (\ex,\ey) ++(45:.4) arc (45:-45:.4);
	\draw (\ex,\ey) ++(45:.2) arc (45:-45:.2);
	
	\draw (\ex,\ey) ++(135:.8) arc (135:225:.8);
	\draw (\ex,\ey) ++(135:.6) arc (135:225:.6);
	\draw (\ex,\ey) ++(135:.4) arc (135:225:.4);
	\draw (\ex,\ey) ++(135:.2) arc (135:225:.2);
	
	\node[text width=3cm] at (3,5.5) {LoraWan\\EU868};	
	
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (0.1,0.1) rectangle (4,3);
	\node[text width=3cm] at (1.7,2.7) {Concentrator};	
	
	
	\filldraw[color=gray!40, draw=black, rounded corners=3pt ] (0,-1) rectangle (16.1,-12.1);
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (0.1,-1.1) rectangle (4, -4);
	\node[text width=5cm] at (2.7,-1.7) {LoraWan-Gateway-\\Bridge};
	\draw[latex-latex] (2,0.1) -> (2,-1.1) node[anchor=north west]{};
	\node[text width=1cm] at (3,-0.5) {\scriptsize UDP 0.0.0.0:1700};
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (6,-1.1) rectangle (10, -4);
	\node[text width=3cm] at (7.8,-1.5) {MQTT-Broker};
	\draw[latex-latex] (4,-2.5) -> (6,-2.5) node[anchor=north west]{};
	\node[text width=1cm] at (4.6,-3) {\scriptsize TCP 127.0.0.1:1883};
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (6,-5) rectangle (10, -8);
	\node[text width=3cm] at (7.8,-5.4) {Network-Server};
	\draw[latex-latex] (8,-4) -> (8,-5) node[anchor=north west]{};
	\node[text width=1cm] at (9,-4.5) {\scriptsize TCP 127.0.0.1:1883};
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (6,-9) rectangle (10, -12);
	\node[text width=3cm] at (7.8,-9.6) {Application-Server};
	\draw[latex-latex] (8,-8) -> (8,-9) node[anchor=north west]{};
	\node[text width=1cm] at (9,-8.5) {\scriptsize TCP 0.0.0.0:8000};
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (0.1,-7) rectangle (4, -10);
	\node[text width=3cm] at (1.8,-7.4) {Postgresql};	
	
	\filldraw[color=gray!70, draw=black, rounded corners=3pt ] (12,-7) rectangle (16, -10);
	\node[text width=3cm] at (14,-7.4) {Redis};

	
	\draw[line width=1pt] (2,-6.5) -- (6,-6.5) node[anchor=north west]{};
	\draw[-latex, line width=1pt] (2,-6.5) -> (2,-7) node[anchor=north west]{};
	\node[text width=1cm] at (3,-6) {\scriptsize TCP 127.0.0.1:5432};	
	
	\draw[line width=1pt] (10,-6.5) -- (14,-6.5) node[anchor=north west]{};
	\draw[-latex, line width=1pt] (14,-6.5) -> (14,-7) node[anchor=north west]{};
	\node[text width=1cm] at (12,-6) {\scriptsize TCP 127.0.0.1:6379};
		
	\draw[line width=1pt] (2,-10.5) -- (6,-10.5) node[anchor=north west]{};
	\draw[-latex, line width=1pt] (2,-10.5) -> (2,-10) node[anchor=north west]{};
	\node[text width=1cm] at (3,-11) {\scriptsize TCP 127.0.0.1:5432};	
	
	\draw[line width=1pt] (10,-10.5) -- (14,-10.5) node[anchor=north west]{};
	\draw[-latex, line width=1pt] (14,-10.5) -> (14,-10) node[anchor=north west]{};
	\node[text width=1cm] at (12,-11) {\scriptsize TCP 127.0.0.1:6379};
	
	\draw[latex-latex, line width=1pt] (8,-12) -> (8, -13) node[anchor=north west]{};
	\node[text width=5cm] at (11,-12.6) {\scriptsize TCP 0.0.0.0:8080};
\end{tikzpicture}

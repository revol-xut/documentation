Climate Tracker with LoRaWAN - Documentation
---------------------------------------------

**Contact**: <revol-xut@protonmail.com> <robo-armin@gmx.de>

This repository does not only contain documentation but also the slides for the presentation, specifications or even the circuit diagrams. Read further for an
general overview of the project.

### Abstract
The goal of this project is to create an easy to setup and reliable system for tracking indoor climate which is determined by CO2 concentration
temperature and air pressure. This data is collected by an energy efficient microcontroller. The transmission of the data is realized by
a the LuraWan standard. On the other is the LuraWan Server waiting. That feeds the received data into a Influx database.

### Structure
The Project is split into two parts in front and behind the LuraWan Gateway.

**[Microcontroller Code](https://bitbucket.org/revol-xut/microcontroller-code/)**

**[ChripStack Application Server](https://bitbucket.org/revol-xut/chripstack-application-server/)**



### Contributers

   - Armin Wolf
   - Tassilo Tanneberger

With technical help from Sebastian Simon, Mayk Röhrich.
